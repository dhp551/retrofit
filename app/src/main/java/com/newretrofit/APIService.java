package com.newretrofit;

import retrofit.Call;
import retrofit.http.GET;

/**
 * Created by kvanadev5 on 15/09/15.
 */
public interface APIService {
    @GET(Constants.IP_API_URL_PATH)
    Call<AddressModels> getAddress();
}
