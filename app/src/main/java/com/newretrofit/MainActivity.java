package com.newretrofit;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

@EActivity(R.layout.activity_main)
public class MainActivity extends AppCompatActivity {
    @ViewById
    TextView timeZone;
    @ViewById
    TextView country;
    @ViewById
    TextView regionName;
    @ViewById
    TextView city;
    @ViewById
    TextView zip;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Network Adapter
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.IP_API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        //API reference
        APIService apiService = retrofit.create(APIService.class);
        //Create call
        Call<AddressModels> call = apiService.getAddress();
        //execute call
        call.enqueue(new Callback<AddressModels>() {
            @Override
            public void onResponse(Response<AddressModels> response) {
                //get our response from body
                AddressModels addressModels = response.body();

                timeZone.setText(addressModels.getZip());
                country.setText(addressModels.getCountry());
                regionName.setText(addressModels.getRegionName());
                city.setText(addressModels.getCity());
                zip.setText(addressModels.getZip());

            }

            @Override
            public void onFailure(Throwable t) {

                city.setText("Something went wrong..");

            }
        });
    }
}
